package eribu.cms.http;

public class BindingError {

    private String objectName;
    private String fieldName;
    private String fieldValue;
    private String errorMessage;

    public BindingError() {
        this.objectName = "";
        this.fieldName = "";
        this.fieldValue = "";
        this.errorMessage = "";
    }

    protected String getObjectName() {
        return objectName;
    }

    protected void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    protected String getFieldName() {
        return fieldName;
    }

    protected void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    protected String getFieldValue() {
        return fieldValue;
    }

    protected void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    protected String getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String error_message) {
        this.errorMessage = error_message;
    }

    @Override
    public String toString() {
        return "BindingError [objectName=" + objectName + ", fieldName=" + fieldName + ", fieldValue=" + fieldValue
                + ", errorMessage=" + errorMessage + "]";
    }
}