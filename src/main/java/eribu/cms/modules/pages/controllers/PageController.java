package eribu.cms.modules.pages.controllers;

import eribu.cms.http.BindingErrorsResponse;
import eribu.cms.modules.pages.models.Page;
import eribu.cms.modules.pages.services.PageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("api/pages")
public class PageController {

    @Autowired
    private PageService pageService;

    @CrossOrigin(origins = "http://localhost:8081")
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Collection<Page>> getPages(){
        Collection<Page> pages = this.pageService.findAll();
        if(pages.isEmpty()){
            return new ResponseEntity<Collection<Page>>(pages, HttpStatus.OK);
        }
        return new ResponseEntity<Collection<Page>>(pages, HttpStatus.OK);
    }
	@CrossOrigin(origins = "http://localhost:8081")
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Page> create(@RequestBody @Valid Page page, BindingResult bindingResult, UriComponentsBuilder ucBuilder){
		BindingErrorsResponse errors = new BindingErrorsResponse();
		HttpHeaders headers = new HttpHeaders();
		if(bindingResult.hasErrors() || (page == null)){
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return new ResponseEntity<Page>(headers, HttpStatus.BAD_REQUEST);
		}
		this.pageService.store(page);
		headers.setLocation(ucBuilder.path("/api/pets/{id}").buildAndExpand(page.getId()).toUri());
		return new ResponseEntity<Page>(page, headers, HttpStatus.CREATED);
	}
}
