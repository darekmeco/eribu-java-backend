package eribu.cms.modules.pages.services;

import eribu.cms.modules.pages.models.Page;
import org.springframework.beans.factory.annotation.Autowired;
import eribu.cms.modules.pages.repositories.PageRepository;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Collection;

@Service
public class PageServiceImpl implements PageService {

    @Autowired
    private PageRepository pageRepository;

    @Autowired
    public PageServiceImpl(PageRepository pageRepository) {
        this.pageRepository = pageRepository;

    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Page> findAll() throws DataAccessException {
        return pageRepository.findAll();
    }

    @Override
    @Transactional
    public void store(Page page) throws DataAccessException {
        pageRepository.save(page);

    }

}
