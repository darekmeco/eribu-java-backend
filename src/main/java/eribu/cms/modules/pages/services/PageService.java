package eribu.cms.modules.pages.services;

import eribu.cms.modules.pages.models.Page;
import org.springframework.dao.DataAccessException;

import java.util.Collection;

public interface PageService {
    Collection<Page> findAll() throws DataAccessException;
    void store(Page page) throws DataAccessException;


}
