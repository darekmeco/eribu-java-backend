package eribu.cms.modules.pages.repositories;

import eribu.cms.modules.pages.models.Page;
import org.springframework.dao.DataAccessException;

import java.util.Collection;

public interface PageRepository {

    Collection<Page> findAll() throws DataAccessException;
    void save(Page page) throws DataAccessException;

}