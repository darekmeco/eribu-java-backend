package eribu.cms.modules.pages.repositories;

import eribu.cms.modules.pages.models.Page;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

@Repository
@Profile("jpa")
public class PageRepositoryImpl implements PageRepository {


    @PersistenceContext
    private EntityManager em;

    @SuppressWarnings("unchecked")
    @Override
    public Collection<Page> findAll() throws DataAccessException {
        return this.em.createQuery("SELECT page FROM Page page").getResultList();
    }


    @Override
    public void save(Page page) {
        if (page.getId() == null) {
            this.em.persist(page);
        } else {
            this.em.merge(page);
        }
    }

}
