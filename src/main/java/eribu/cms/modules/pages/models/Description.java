package eribu.cms.modules.pages.models;
import javax.persistence.Embeddable;

@Embeddable
public class Description {

    private String description;

    private String shortDescription;
}
