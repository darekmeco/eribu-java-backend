package eribu.cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
/*@ComponentScan({
        "eribu.cms.modules.pages.repositories",
        "eribu.cms.modules.pages.services",
        "eribu.cms.modules.pages.controllers",
        "eribu.cms.security"
})*/

@ComponentScan(basePackages = {"eribu.cms.modules.pages", "eribu.cms.security"})
@EnableJpaRepositories(basePackages={"eribu.cms.modules.pages.repositories"})
//@EnableJpaRepositories("eribu.cms.modules.pages.repositories")

public class CmsApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CmsApplication.class);
    }


    public static void main(String[] args) {

        SpringApplication.run(CmsApplication.class, args);

    }
}
